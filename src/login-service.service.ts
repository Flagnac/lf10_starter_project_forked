import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {
  private apiUrl = 'http://authproxy.szut.dev';
  public result = false;
  public trys = 0;
  public locked = false;
  private tokenS: string = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIzUFQ0dldiNno5MnlQWk1EWnBqT1U0RjFVN0lwNi1ELUlqQWVGczJPbGU0In0.eyJleHAiOjE2NDQ2MTI0NTgsImlhdCI6MTY0NDU5ODA1OCwianRpIjoiOWE4ODFhMDUtOGI0ZS00NDczLWIwZjYtZGM1ODdkZjIwYmZiIiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5zenV0LmRldi9hdXRoL3JlYWxtcy9zenV0IiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjU1NDZjZDIxLTk4NTQtNDMyZi1hNDY3LTRkZTNlZWRmNTg4OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVtcGxveWVlLW1hbmFnZW1lbnQtc2VydmljZSIsInNlc3Npb25fc3RhdGUiOiI3ODc1MTE5OC1kNjA4LTQxNjItOTU3NS1mNDJlNzNlNzI1YjgiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiZGVmYXVsdC1yb2xlcy1zenV0IiwidW1hX2F1dGhvcml6YXRpb24iLCJ1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJ1c2VyIn0.MnFXaWXtpWSIPhOWVQ4VM2rfC3PsEteGnIcHRBgE-EgGHiVQ-aD3KCNBYex5tfvukMhBlN88juTa82UU3XFnFc_R8KHANIYpqTMIaaDb3-pHAVySo8uzEJDefOzTBf_QmOBj4ssLPEReVBbEdq__7uZP_KjhVdLbPUECMUVO96fkiHJ4Oyx070i87YaR3sLnjsLA_gbHZkFbRyJe-u556Gces2LyyS6h5E2U8_zCtt7DOBf077JYW0euO9to1TQfNvYXSyWaUSbo2BMrkdOcifafye11KM9rEWbcLP4tU5AMywONDwUGu_sB2SXGwHJO1H2Jj7rDtJyetrZzP3_aKw';
  constructor(
    private httpClient: HttpClient
  ) { }

  async login(username:string, password:string){

    if(this.locked){
      let counter = 0;
      while(counter< 120000){
        counter ++;
      }
    }
    //await this.call(username,password);

    if(username == 'user' && password == 'test'){
      this.result = true;
    }
    else{
      this.result = false;
      this.trys++;
    }//workaround um zu spät kommenden login call

    if(this.result = false){
      this.trys++;
    }
    if(this.trys == 3){
      this.locked = true;
    }
  }

  getToken():string{
    return this.tokenS;
  }

  getResult():boolean{
    return this.result;
  }

  getLockedState():boolean{
    return this.locked;
  }

  private getBearerFromBody(body:any):string{
    let params = JSON.stringify(body).split(',');
    let access_token = params[0].substring(params[0].indexOf(':')+2, params[0].length-1);
    return access_token;
  }

  async call(username:string, password:string){
    let header = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded')
    let params = new HttpParams().set('username', username).set('password', password).set('grant_type', 'password').set('client_id', 'employee-management-service')
    await this.httpClient.post<any>(this.apiUrl,params,{headers:header, observe: 'response'}).subscribe(data =>{
      let responseBody = data.body;
      let bearer = this.getBearerFromBody(responseBody);
      if(bearer != null){
        this.result = true;
      }
      else{
        this.result = false;
      }
    })
  }

}
