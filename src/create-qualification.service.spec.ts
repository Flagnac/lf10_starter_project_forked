import { TestBed } from '@angular/core/testing';

import { CreateQualificationService } from './create-qualification.service';

describe('CreateQualificationService', () => {
  let service: CreateQualificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreateQualificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
