import {Qualification} from "./employee-overview-form/models/Qualification";

export const QUALIFICATIONS: Qualification[] = [
  {designation:"Java"},
  {designation:"C#"},
  {designation:"angular"}
]
