import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import { CreateQualificationFormComponent } from './createQualificationForm/createQualificationForm.component';
import {FormsModule} from "@angular/forms";
import { LoginFormComponent } from './login-form/login-form.component';
import { EmployeeOverviewFormComponent } from './employee-overview-form/employee-overview-form.component';
import {RouterModule} from "@angular/router";
import { LogoutFormComponent } from './employee-overview-form/logout-form/logout-form.component';
import { CreateEmployeeFormComponent } from './create-employee-form/create-employee-form.component';
import {QualificationdetailsComponent} from "./qualificationdetails/qualificationdetails.component";

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    EmployeeOverviewFormComponent,
    CreateQualificationFormComponent,
    LogoutFormComponent,
    CreateEmployeeFormComponent,
    QualificationdetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    RouterModule.forRoot([
      { path: '', component: LoginFormComponent},
      { path: 'logout', component: LogoutFormComponent},
      { path: 'home', component: EmployeeOverviewFormComponent},
      { path: 'createQualification', component: CreateQualificationFormComponent},
      { path: 'createEmployee', component: CreateEmployeeFormComponent},
      { path: 'allQualifications', component: QualificationdetailsComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
