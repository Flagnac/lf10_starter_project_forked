import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateQualificationFormComponent } from './createQualificationForm.component';

describe('QualificationsComponent', () => {
  let component: CreateQualificationFormComponent;
  let fixture: ComponentFixture<CreateQualificationFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateQualificationFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateQualificationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
