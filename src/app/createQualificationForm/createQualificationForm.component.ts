import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {CreateQualificationService} from "../../create-qualification.service"

@Component({
  selector: 'app-createQualificationForm',
  templateUrl: './createQualificationForm.component.html',
  styleUrls: ['./createQualificationForm.component.css']
})
export class CreateQualificationFormComponent implements OnInit {
  qualificationName: string = '';

  constructor(private createQualification:CreateQualificationService) {
  }

  ngOnInit(): void {
  }

  post(qualificationName: string) {
    this.createQualification.post(qualificationName)
  }
}
