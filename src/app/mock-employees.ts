import {Employee} from "./employee-overview-form/models/Employee";

export const EMPLOYEES: Employee[] = [
  {id: 1, lastname:"Peters", firstname:"Mona", street:"Frenchstreet", postcode:"28881", city:"berlin", phone:"123456789", qualifications:[{designation:"Java"},{designation: "C#"}]},
  {id: 2, lastname:"Peters2", firstname:"Mona2", street:"Frenchstreet", postcode:"28881", city:"berlin", phone:"12345678", qualifications:[{designation:"C#"}]},
  {id: 3, lastname:"Peters3", firstname:"Mona3", street:"Frenchstreet", postcode:"28881", city:"berlin", phone:"1234567", qualifications:[{designation:"Java"}]},
  {id: 4, lastname:"Peters4", firstname:"Mona4", street:"Frenchstreet", postcode:"28881", city:"berlin", phone:"123456", qualifications:[{designation:"Java"}]},
  {id: 5, lastname:"Peters5", firstname:"Mona5", street:"Frenchstreet", postcode:"28881", city:"berlin", phone:"12345", qualifications:[{designation:"C#"}]}
]
