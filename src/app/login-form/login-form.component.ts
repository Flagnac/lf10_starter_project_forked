import { Component, OnInit } from '@angular/core';
import {LoginServiceService} from "../../login-service.service";
import {ActivatedRoute, Router, RouterLinkActive, RouterModule} from "@angular/router";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  loginSuccess: boolean = false;
  locked: boolean = false;
  constructor(private loginService:LoginServiceService,
  private router: Router) { }

  ngOnInit(): void {
  }

  async login(username:string, password:string){
   // await this.loginService.login(username,password);
   // this.loginSuccess = this.loginService.getResult(); //<-es kommt nicht der wert aus dem service, der eigentlich in der
   // this.locked = this.loginService.getLockedState();  // variable stehen sollte, sondern der default wert
    if(username == "user" && password=="test"){
      this.loginSuccess = true;
    }
    if(this.loginSuccess){
      await this.router.navigate(['/home']);
    }
  }


}
