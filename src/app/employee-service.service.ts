import { Injectable } from '@angular/core';

import {HttpClient, HttpHeaders} from "@angular/common/http";
import {LoginServiceService} from "../login-service.service";
import {delay, Observable, timeout} from "rxjs";
import {Employee} from "./Employee";

@Injectable({
  providedIn: 'root'
})
export class EmployeeServiceService {

  private apiUrl = 'http://localhost:8089/employees';
  token:any = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIzUFQ0dldiNno5MnlQWk1EWnBqT1U0RjFVN0lwNi1ELUlqQWVGczJPbGU0In0.eyJleHAiOjE2NDQ2MTI0NTgsImlhdCI6MTY0NDU5ODA1OCwianRpIjoiOWE4ODFhMDUtOGI0ZS00NDczLWIwZjYtZGM1ODdkZjIwYmZiIiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5zenV0LmRldi9hdXRoL3JlYWxtcy9zenV0IiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjU1NDZjZDIxLTk4NTQtNDMyZi1hNDY3LTRkZTNlZWRmNTg4OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVtcGxveWVlLW1hbmFnZW1lbnQtc2VydmljZSIsInNlc3Npb25fc3RhdGUiOiI3ODc1MTE5OC1kNjA4LTQxNjItOTU3NS1mNDJlNzNlNzI1YjgiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiZGVmYXVsdC1yb2xlcy1zenV0IiwidW1hX2F1dGhvcml6YXRpb24iLCJ1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJ1c2VyIn0.MnFXaWXtpWSIPhOWVQ4VM2rfC3PsEteGnIcHRBgE-EgGHiVQ-aD3KCNBYex5tfvukMhBlN88juTa82UU3XFnFc_R8KHANIYpqTMIaaDb3-pHAVySo8uzEJDefOzTBf_QmOBj4ssLPEReVBbEdq__7uZP_KjhVdLbPUECMUVO96fkiHJ4Oyx070i87YaR3sLnjsLA_gbHZkFbRyJe-u556Gces2LyyS6h5E2U8_zCtt7DOBf077JYW0euO9to1TQfNvYXSyWaUSbo2BMrkdOcifafye11KM9rEWbcLP4tU5AMywONDwUGu_sB2SXGwHJO1H2Jj7rDtJyetrZzP3_aKw';
  constructor(
    private httpClient: HttpClient,
    private loginService: LoginServiceService
  ) {}

  allEmployees():Observable<Employee[]>{
    this.setToken();
    let employees = this.httpClient.get<Employee[]>(this.apiUrl, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.token}`)
    });
    return employees;
  }

  async saveEmployee(toSave:Employee){
    await this.setToken();
    this.httpClient.post(this.apiUrl, toSave ,{
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.token}`)
    });
  }

  async setToken(){
    await this.loginService.login('user', 'test');
    this.token = this.loginService.getToken();
  }



}
