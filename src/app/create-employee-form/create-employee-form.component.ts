import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {EmployeeServiceService} from "../employee-service.service";
import {Employee} from "../Employee";



@Component({
  selector: 'app-create-employee-form',
  templateUrl: './create-employee-form.component.html',
  styleUrls: ['./create-employee-form.component.css']
})
export class CreateEmployeeFormComponent implements OnInit {

  constructor(private employeeService:EmployeeServiceService) { }

  ngOnInit(): void {
  }


  save(lastname:string, firstname:string, street:string, postcode:string, city:string, phone:string){
    let employee = {lastName:lastname, firstName:firstname, street:street, postcode:postcode, city:city, phone:phone};
    this.employeeService.saveEmployee(employee);
  }

}
