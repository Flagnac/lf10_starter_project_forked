import { Component, OnInit } from '@angular/core';
import {LoginServiceService} from "../../login-service.service";
import {Observable, of} from "rxjs";
import {QualificationServiceService} from "../qualification-service.service";
import {Qualification} from "../employee-overview-form/models/Qualification";
import {QUALIFICATIONS} from "../mock-qualifications";

@Component({
  selector: 'app-qualificationdetails',
  templateUrl: './qualificationdetails.component.html',
  styleUrls: ['./qualificationdetails.component.css']
})
export class QualificationdetailsComponent implements OnInit {
  qualification$: Observable<Qualification[]>;
  test:any = 'Did not work';

  constructor(private qualificationService: QualificationServiceService,
              private loginService: LoginServiceService) {
    this.qualification$ = of([]);
  }

  ngOnInit(): void {
    //this.qualification$ = this.qualificationService.allQualifications();
    this.qualification$ = of(QUALIFICATIONS);
  }

  testing(){
    this.test = this.loginService.getToken();
  }

}
