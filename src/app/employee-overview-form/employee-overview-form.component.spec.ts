import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeOverviewFormComponent } from './employee-overview-form.component';

describe('EmployeeOverviewFormComponent', () => {
  let component: EmployeeOverviewFormComponent;
  let fixture: ComponentFixture<EmployeeOverviewFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeOverviewFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeOverviewFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
