import { Component, OnInit } from '@angular/core';

import {Observable, of} from "rxjs";
import {EmployeeServiceService} from "../employee-service.service";
import {Employee} from "./models/Employee";
import {EMPLOYEES} from "../mock-employees";

@Component({
  selector: 'app-employee-overview-form',
  templateUrl: './employee-overview-form.component.html',
  styleUrls: ['./employee-overview-form.component.css']
})
export class EmployeeOverviewFormComponent implements OnInit {
  employees$: Observable<Employee[]>;
  test:any = 'Didnt Work';
  constructor(
    private employeesService:EmployeeServiceService
  ) {
    this.employees$ = of([]);
  }

  ngOnInit(): void {
   // this.employees$ = this.employeesService.allEmployees();
    this.employees$ = of(EMPLOYEES);
  }
}
