import {Qualification} from "./Qualification";

export interface Employee{
  id:number,
  lastname:string,
  firstname:string,
  street:string,
  postcode:string,
  city:string,
  phone:string
  qualifications:Qualification[]
}
