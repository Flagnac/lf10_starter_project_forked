import { Injectable } from '@angular/core';
import {LoginServiceService} from "../login-service.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Qualification} from "./employee-overview-form/models/Qualification";


@Injectable({
  providedIn: 'root'
})
export class QualificationServiceService {

  private apiUrl = '/backend/';
  token:any = '';

  constructor(private httpClient: HttpClient,
              private loginService: LoginServiceService){}

  allQualifications():Observable<Qualification[]>{
    this.setToken();
    let header = new HttpHeaders()
      .set('Authorization', `Bearer ${this.token}`)
      .set('Content-Type', 'application/json');
    let urlAddition = 'qualification';
    let qualification = this.httpClient.get<Qualification[]>(this.apiUrl+urlAddition,{headers: header});
    return qualification;
  }
  setToken(){
    this.token = this.loginService.getToken();
  }
}
