import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Qualification} from "./app/employee-overview-form/models/Qualification";
import {Employee} from "./app/Employee";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CreateQualificationService{
  //bearer holen
  private bearer: string = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIzUFQ0dldiNno5MnlQWk1EWnBqT1U0RjFVN0lwNi1ELUlqQWVGczJPbGU0In0.eyJleHAiOjE2NDQ2MTUyMTMsImlhdCI6MTY0NDYwMDgxMywianRpIjoiNDQyZjIwOWUtZjgxNy00OWVlLWFkMDItODhhNmU4OWUzNWU4IiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5zenV0LmRldi9hdXRoL3JlYWxtcy9zenV0IiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjU1NDZjZDIxLTk4NTQtNDMyZi1hNDY3LTRkZTNlZWRmNTg4OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVtcGxveWVlLW1hbmFnZW1lbnQtc2VydmljZSIsInNlc3Npb25fc3RhdGUiOiIwZjk3MGQ0NC1iMjliLTQ5M2QtYjI0Yy1hY2NmOTIyYjdjMTUiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiZGVmYXVsdC1yb2xlcy1zenV0IiwidW1hX2F1dGhvcml6YXRpb24iLCJ1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJ1c2VyIn0.fwqgaKizbz7V7lR-H6zdbYwwAlevHhuZe-9DQ9iuF2YDqvMhggbPBdWjxkuN7NICHlZrKeHLwZ3izQl3HTxCz2FHCKIEwWtgS9MbzCIJSZacAI34_4itvbkY1Os7exQbrcu4Ji9-2dKCOdueIq7Mv2bC0W68gyh38vYPTtz9h1YAywDQUevV1MAZ_BYYqBw9qAj18LbmEbcAuP-o3RsbxzI_OxPkRl2lAV2C25Od-eox6MB5S0BMd2PsHpyoVFCm-rj8Xw7-YnTlIQ4kGjdSg-TyVs3momYWSDOV8RH3aHfQRb1SIp9s82S5tTBrxtC4RTFfmoW7doQ2d18URh5p6g';
  private quali: Qualification |undefined;
  private qualification: Observable<Qualification[]> | undefined;
  constructor(private httpClient: HttpClient) {
  }

  // post2(qualificationName: string) {
  //   this.quali = {designation : qualificationName}
  //   let header = new HttpHeaders()
  //     .set('accept', 'application/json')
  //     .set('Content-Type','application/json')
  //     .set('Access-Control-Allow-Origin', '*')
  //     .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  //     .set('Authorization', 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIzUFQ0dldiNno5MnlQWk1EWnBqT1U0RjFVN0lwNi1ELUlqQWVGczJPbGU0In0.eyJleHAiOjE2NDQ2MTUyMTMsImlhdCI6MTY0NDYwMDgxMywianRpIjoiNDQyZjIwOWUtZjgxNy00OWVlLWFkMDItODhhNmU4OWUzNWU4IiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5zenV0LmRldi9hdXRoL3JlYWxtcy9zenV0IiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjU1NDZjZDIxLTk4NTQtNDMyZi1hNDY3LTRkZTNlZWRmNTg4OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVtcGxveWVlLW1hbmFnZW1lbnQtc2VydmljZSIsInNlc3Npb25fc3RhdGUiOiIwZjk3MGQ0NC1iMjliLTQ5M2QtYjI0Yy1hY2NmOTIyYjdjMTUiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiZGVmYXVsdC1yb2xlcy1zenV0IiwidW1hX2F1dGhvcml6YXRpb24iLCJ1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJ1c2VyIn0.fwqgaKizbz7V7lR-H6zdbYwwAlevHhuZe-9DQ9iuF2YDqvMhggbPBdWjxkuN7NICHlZrKeHLwZ3izQl3HTxCz2FHCKIEwWtgS9MbzCIJSZacAI34_4itvbkY1Os7exQbrcu4Ji9-2dKCOdueIq7Mv2bC0W68gyh38vYPTtz9h1YAywDQUevV1MAZ_BYYqBw9qAj18LbmEbcAuP-o3RsbxzI_OxPkRl2lAV2C25Od-eox6MB5S0BMd2PsHpyoVFCm-rj8Xw7-YnTlIQ4kGjdSg-TyVs3momYWSDOV8RH3aHfQRb1SIp9s82S5tTBrxtC4RTFfmoW7doQ2d18URh5p6g')
  //   console.log(this.quali)
  //   console.log(JSON.stringify(this.quali))
  //   this.httpClient.post<Qualification>(this.apiUrl, JSON.stringify(this.quali), {headers: header})
  //     .subscribe(data => console.log(data.designation))
  // }

  post(qualificationName: string) {
    this.quali = {designation : qualificationName}

    this.httpClient.post<Qualification>('/backend', JSON.stringify(this.quali) ,{
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Authorization', `Bearer ${this.bearer}`)})
      .subscribe(data => console.log(data.designation));
  }

}
